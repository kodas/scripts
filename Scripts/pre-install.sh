# Format partitions
    mkfs.vfat -n "EFI System" /dev/nvme0n1p1
    mkfs.ext4 -L boot /dev/nvme0n1p2
    mkfs.ext4 -L root /dev/nvme0n1p3

# Encryption support
    modprobe dm-crypt
    modprobe dm-mod

# Encryption setup
    cryptsetup luksFormat -v -s 512 -h sha512 /dev/nvme0n1p3
    cryptsetup open /dev/nvme0n1p3 archlinux

# Format encrypted partitions
    mkfs.btrfs -L root /dev/mapper/archlinux

# Mounting & setting up the btrfs filesystem
    1. Mounting
        mount -t btrfs /dev/mapper/archlinux /mnt

    2. Creating subvolumes
        cd /mnt
        btrfs subvolume create @ /mnt
        btrfs subvolume create @
        btrfs subvolume create @home
        btrfs subvolume create @swap
    
    3. Mounting subvolumes
        cd /
        umount -R /mnt
        mount -t btrfs -o subvol=@ /dev/mapper/archlinux /mnt
        mkdir /mnt/home
        mount -t btrfs -o subvol=@home /dev/mapper/archlinux /mnt/home
        mkdir /mnt/swap
        mount -t btrfs -o subvol=@swap /dev/mapper/archlinux /mnt/swap
    
# Setting up GRUB
    mkdir /mnt/boot
    mount /dev/nvme0n1p2 /mnt/boot
    mkdir /mnt/boot/efi
    mount /dev/nvme0n1p1 /mnt/boot/efi

# Setting up Swap
    cd /mnt
    touch /mnt/swap/swapfile
    chattr +C /mnt/swap/swapfile
    fallocate /mnt/swap/swapfile -l 6944M
    mkswap /mnt/swap/swapfile
    swapon /mnt/swap/swapfile
    chmod 0600 /mnt/swap/swapfile

# Install System
    pacstrap -i /mnt base base-devel linux linux-firmware grub efibootmgr networkmanager sudo nano

# Genfstab
    genfstab -U /mnt > /mnt/etc/fstab
